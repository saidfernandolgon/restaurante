/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author USUARIO
 */
public class Plato {
    
    private String id;
    private String detalles;
    private int precio;

    public Plato() {
    }

    public Plato(String id, String detalles, int precio) {
        this.id = id;
        this.detalles = detalles;
        this.precio = precio;
    }

    public String getId() {
        return id;
    }

    public String getDetalles() {
        return detalles;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDetalles(String detalles) {
        this.detalles = detalles;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }
    
    
}
