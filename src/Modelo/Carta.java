/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author USUARIO
 */
public class Carta {
    
    private String id;
    private Plato platos[];
    private int canPlatos = 0;

    public Carta() {
    }

    public Carta(String id, Plato[] platos) {
        this.id = id;
        this.platos = platos;
    }

    public String getId() {
        return id;
    }

    public Plato[] getPlatos() {
        return platos;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNuevoPlato(Plato nuevo) {
        
        if(canPlatos<platos.length){
            for(int i=0; i<platos.length; i++){
                if(platos[i]==null){
                    platos[i]=nuevo;
                    this.canPlatos++;
                    i=platos.length;
                }
            }
        }
        
    }
    
}
