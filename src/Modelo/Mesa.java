/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author USUARIO
 */
public class Mesa {
    
    private String id;
    private boolean apartada=false;
    private Cliente cliente;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
    public boolean isApartada() {
        return apartada;
    }

    public String getId() {
        return id;
    }

    public void setApartada(boolean apartada) {
        this.apartada = apartada;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    
}
